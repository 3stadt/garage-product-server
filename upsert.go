package main

import (
	"encoding/json"
	"errors"
	bolt "go.etcd.io/bbolt"
	"io/ioutil"
	"net/http"
)

func upsertProducts(w http.ResponseWriter, r *http.Request) {
	id := r.Header.Get("X-ID")
	if id == "" {
		w.WriteHeader(http.StatusBadRequest)
		sendError(w, errors.New("header X-ID is missing"))
		return
	}
	content, err := ioutil.ReadAll(r.Body)
	if err != nil {
		sendError(w, err)
		return
	}
	pr := ProductUpsertRequest{}
	err = json.Unmarshal(content, &pr)
	if err != nil {
		sendError(w, err)
		return
	}
	err = db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(id))
		if b == nil {
			return errors.New("X-ID not found")
		}
		for _, product := range pr.Products {
			j, _ := json.Marshal(product)
			err := b.Put([]byte(product.ID), j)
			if err != nil {
				return err
			}
		}
		return nil
	})
	if err != nil {
		sendError(w, err)
		return
	}
	sendSuccess(w, "products written")
}
