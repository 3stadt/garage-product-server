package main

import (
	"fmt"
	bolt "go.etcd.io/bbolt"
	"os"
	"time"
)

var db *bolt.DB

func init() {
	var err error
	db, err = bolt.Open("my.db", 0600, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
