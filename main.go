package main

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"time"
)

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(""))
	})
	r.HandleFunc("/register", registerUser).Methods("POST")
	r.HandleFunc("/products/upsert", upsertProducts).Methods("POST")
	r.HandleFunc("/products/get", readProducts).Methods("GET")
	r.HandleFunc("/products/delete", deleteProducts).Methods("DELETE")
	srv := &http.Server{
		Handler:      r,
		Addr:         ":8000",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Fatal(srv.ListenAndServe())
}
