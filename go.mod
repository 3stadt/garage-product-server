module garage-product-server

go 1.14

require (
	github.com/aidarkhanov/nanoid v1.0.4
	github.com/gorilla/mux v1.7.4
	go.etcd.io/bbolt v1.3.4
)
