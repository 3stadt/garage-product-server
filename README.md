# Purpose

Simple API providing access to a simpmle product database.

The project is used as a demo API to learn stuff about the topic.

# Usage

## Available API endpoints

### Create a new catalog

**Endpoint:** `/register`   

**Method:** `POST`   

**Request Header:** None

**Request Body:**

`None`

**Response:**

```json
{
  "error": false,
  "id": "abcd123"
}
```

### Insert/Update products

**Endpoint:** `/products/upsert`

**Method:** `POST`

**Request Header:** `X-ID: abcd123`

**Request Body:**

```json
{
  "products": [
    {
      "id": "prod-001",
      "name": "Product Name",
      "price": 0.99,
      "amount": 100,
      "unit": "Kilo",
      "stock": 20
    }
  ]
}
```

**Response:**


```json
{
  "error": false,
  "message": "products written"
}
```

### Read all products

**Endpoint:** `/products/get`

**Method:** `GET`

**Request Header:** `X-ID: abcd123`

**Request Body:**

`None`

**Response:**

```json
{
  "error": false,
  "products": [
    {
      "id": "prod-001",
      "name": "Product Name",
      "price": 0.99,
      "amount": 100,
      "unit": "Kilo",
      "stock": 20
    }
  ]
}
```