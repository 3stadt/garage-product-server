package main

import (
	"encoding/json"
	"github.com/aidarkhanov/nanoid"
	bolt "go.etcd.io/bbolt"
	"net/http"
)

func registerUser(w http.ResponseWriter, r *http.Request) {
	id := nanoid.New()
	err := db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte(id))
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			sendError(w, err)
			return nil
		}
		return nil
	})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		sendError(w, err)
		return
	}
	resp, _ := json.Marshal(Registration{
		Error: false,
		ID:    id,
	})
	_, _ = w.Write(resp)
}
