package main

type Registration struct {
	Error bool   `json:"error"`
	ID    string `json:"id"`
}

type ProductCollection []Product

type ProductDeleteRequest struct {
	ProductIDs []string `json:"product-ids"`
}

type ProductUpsertRequest struct {
	Products ProductCollection
}

type Product struct {
	ID     string  `json:"id"`
	Name   string  `json:"name"`
	Price  float64 `json:"price"`
	Amount int     `json:"amount"`
	Unit   string  `json:"unit"`
	Stock  uint8   `json:"stock"`
}

type ErrorResponse struct {
	Error   bool
	Message string
}
