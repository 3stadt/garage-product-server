package main

import (
	"encoding/json"
	"errors"
	bolt "go.etcd.io/bbolt"
	"io/ioutil"
	"net/http"
)

func deleteProducts(w http.ResponseWriter, r *http.Request) {
	id := r.Header.Get("X-ID")
	if id == "" {
		w.WriteHeader(http.StatusBadRequest)
		sendError(w, errors.New("header X-ID is missing"))
		return
	}
	content, err := ioutil.ReadAll(r.Body)
	if err != nil {
		sendError(w, err)
		return
	}
	pd := ProductDeleteRequest{}
	err = json.Unmarshal(content, &pd)
	if err != nil {
		sendError(w, err)
		return
	}
	err = db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(id))
		if b == nil {
			return errors.New("X-ID not found")
		}
		for _, id := range pd.ProductIDs {
			err := b.Delete([]byte(id))
			if err != nil {
				return err
			}
		}
		return nil
	})
	if err != nil {
		sendError(w, err)
		return
	}
	sendSuccess(w, "products deleted")
}
