package main

import (
	"errors"
	"fmt"
	bolt "go.etcd.io/bbolt"
	"net/http"
	"strings"
)

func readProducts(w http.ResponseWriter, r *http.Request) {
	id := r.Header.Get("X-ID")
	if id == "" {
		w.WriteHeader(http.StatusBadRequest)
		sendError(w, errors.New("header X-ID is missing"))
		return
	}
	var pr []string
	err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(id))
		c := b.Cursor()
		for k, v := c.First(); k != nil; k, v = c.Next() {
			pr = append(pr, string(v))
		}
		return nil
	})
	if err != nil {
		sendError(w, err)
		return
	}
	_, _ = w.Write([]byte(fmt.Sprintf("{\"error\":false,\"products\": [%s]}", strings.Join(pr, ","))))
}
