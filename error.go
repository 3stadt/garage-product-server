package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)


func sendSuccess(w http.ResponseWriter, message string) {
	resp, _ := json.Marshal(&ErrorResponse{
		Error:   false,
		Message: message,
	})
	_, err := w.Write(resp)
	if err != nil {
		fmt.Printf("[ERROR] %s", err.Error())
	}
}

func sendError(w http.ResponseWriter, e error) {
	resp, _ := json.Marshal(&ErrorResponse{
		Error:   true,
		Message: e.Error(),
	})
	_, err := w.Write(resp)
	if err != nil {
		fmt.Printf("[ERROR] %s", err.Error())
	}
}
